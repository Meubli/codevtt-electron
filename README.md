# Lancer l'appli en développement

```bash
docker-compose down
docker-compose up -d
npm run start
```

- La première commande s'assure que les conteneurs sont bien détruits.
- La deuxieme commande permet de lancer la base de donnée et le serveur codevtt.
- La troisieme commande permet de lancer l'appli electron. (application web et application 'lourde')

Vous pouvez accèder à codevtt à l'adresse suivante: http://localhost/codevtt

Utilisateur: administrator
Mot de passe: root

# Construire un livrable

- Livrable pour windows

```bash
npm run electron:build
```

- Livrable pour linux

```bash
npm run electron:buildlinux
```
