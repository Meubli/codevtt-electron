import { Component } from '@angular/core';
import { ElectronService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { APP_CONFIG } from '../environments/environment';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { ConfigService } from './core/services/config.service';
import { CodevttService } from './core/services/codevtt.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    constructor(
        private config: PrimeNGConfig,
        private electronService: ElectronService,
        private translate: TranslateService,
        private readonly codevttService: CodevttService,
        private readonly configService: ConfigService,
        private readonly router: Router,
        private readonly messageService: MessageService
    ) {
        this.translate.setDefaultLang('fr');
        console.log('APP_CONFIG', APP_CONFIG);

        if (electronService.isElectron) {
            console.log(process.env);
            console.log('Run in electron');
            console.log(
                'Electron ipcRenderer',
                this.electronService.ipcRenderer
            );
            console.log(
                'NodeJS childProcess',
                this.electronService.childProcess
            );
        } else {
            console.log('Run in browser');
        }

        this.translate
            .get('primeng')
            .subscribe((res) => this.config.setTranslation(res));
    }
}
