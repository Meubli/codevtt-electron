import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import {AccordionModule} from 'primeng/accordion';
import {Router, RouterModule} from '@angular/router';
import {CdkDragDrop, DragDropModule, moveItemInArray,} from '@angular/cdk/drag-drop';
import {FormArray, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators,} from '@angular/forms';
import {Bug, Config, ConfigService, Job,} from '../core/services/config.service';
import {MessageService} from 'primeng/api';
import {ClipboardService} from 'ngx-clipboard';
import {ListboxModule} from 'primeng/listbox';
import {ColorPickerModule} from 'ngx-color-picker';
import {CheckboxModule} from 'primeng/checkbox';

@Component({
    selector: 'app-configuration',
    standalone: true,
    imports: [
        CommonModule,
        CheckboxModule,
        ButtonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        AccordionModule,
        DragDropModule,
        DialogModule,
        ColorPickerModule,
        ListboxModule,
    ],
    templateUrl: './configuration.component.html',
    styleUrls: ['./configuration.component.scss'],
})
export class ConfigurationComponent {
    configForm = new FormGroup({
        durations: new FormArray<
            FormGroup<{
                value: FormControl<number>;
                label: FormControl<string>;
                index: FormControl<number>;
            }>
        >([], Validators.required),
        displayWeekEnd: new FormControl(false, Validators.required)
    });

    bugs: Bug[] = [];
    jobs: Job[] = [];

    allBugs: Bug[] = [];
    bugsToAdd: Bug[] = [];

    importedConfig: string;
    showImportConfig = false;
    showAddBug = false;
    loadingTeams = false;

    allDurationsLength = 0;

    constructor(
        private readonly configService: ConfigService,
        private readonly messageService: MessageService,
        private readonly router: Router,
        private readonly clipBoradService: ClipboardService
    ) {
        this.init();

        if (!this.configService.areTeamIssuesFetched()) {
            this.loadingTeams = true;
            let indexL = 0;
            for (const team of this.configService.getTeams()) {
                this.configService
                    .fetchBugsAndJobsForTeam(team)
                    .subscribe(() => {
                        indexL++;
                        if (indexL === this.configService.getTeams().length) {
                            this.loadingTeams = false;
                            this.jobs = this.configService.getAllJobs();
                            console.log(
                                JSON.parse(localStorage.getItem('teams'))
                            );
                        }
                    });
            }
        }
    }

    clickShowAddBug(): void {
        this.showAddBug = true;
        this.allBugs = this.configService.getAllUserIssues().slice();

        for (const bug of this.bugs) {
            const index = this.allBugs.findIndex((b) => b.id === bug.id);

            if (index !== -1) {
                this.allBugs.splice(index, 1);
            }
        }
    }

    dropBugs(event: CdkDragDrop<Bug[]>): void {
        moveItemInArray(this.bugs, event.previousIndex, event.currentIndex);
    }

    dropJobs(event: CdkDragDrop<Job[]>): void {
        moveItemInArray(this.jobs, event.previousIndex, event.currentIndex);
    }

    dropDurations(
        event: CdkDragDrop<
            FormGroup<{
                value: FormControl<number>;
                label: FormControl<string>;
            }>[]
        >
    ): void {
        moveItemInArray(
            this.configForm.controls.durations.controls,
            event.previousIndex,
            event.currentIndex
        );
    }

    initDurations(forceDefault = false): void {

        this.configForm.controls.durations.clear();
        this.allDurationsLength = this.configService.getAllDurations().length;
        let durations =
            this.configService.config.durations &&
            this.configService.config.durations.length > 0
                ? this.configService.config.durations
                : this.configService.getAllDurations();

        if (forceDefault) {
            durations = this.configService.getAllDurations();
        }
        durations?.forEach((b) =>
            this.addDuration(b.value, b.label)
        );
    }

    init(): void {
        this.initDurations();

        if (this.configService.config.bugs) {
            this.bugs = this.configService.config.bugs;
        }

        if (this.configService.config.displayWeekEnd) {
            this.configForm.controls.displayWeekEnd.patchValue(this.configService.config.displayWeekEnd);
        }

        if (this.configService.getAllJobs()) {
            this.jobs = this.configService.getAllJobs();
        }

    }

    submit() {
        this.configForm.controls.durations.controls.forEach(
            (element, index) => {
                element.controls.index.patchValue(index);
            }
        );

        const config: Config = this.configForm.value;
        config.bugs = this.bugs;
        this.configService.setAllJobs(this.jobs);
        this.configService.setConfig(config);
        this.messageService.add({severity: 'success', summary: 'Enregistré'});
        this.router.navigate(['/home']);
    }

    addBugs(): void {
        this.bugs.push(...this.bugsToAdd);
        this.showAddBug = false;
        this.bugsToAdd = [];
    }

    addDuration(value: number, label: string) {
        this.configForm.controls.durations.push(
            new FormGroup({
                value: new FormControl(value, [
                    Validators.required,
                    Validators.max(1.5),
                    Validators.min(0),
                ]),
                label: new FormControl(label, Validators.required),
                index: new FormControl(value),
            })
        );
    }

    isValid(): boolean {
        return this.configForm.valid;
    }

    removeBug(index: number): void {
        this.bugs.splice(index, 1);
    }

    removeDuration(index: number): void {
        this.configForm.controls.durations.removeAt(index);
    }

    importConfig(config: string): void {
        try {
            const c: Config = JSON.parse(config);
            this.configForm.patchValue(c);
            this.importedConfig = null;
            this.showImportConfig = false;
        } catch (error) {
            this.messageService.add({
                severity: 'error',
                summary: 'Format incorrect',
                detail: 'Format json attendu',
            });
        }
    }

    exportConfig(): void {
        this.clipBoradService.copyFromContent(
            JSON.stringify(this.configForm.value)
        );

        this.messageService.add({
            severity: 'succès',
            summary: 'Copié !',
            detail: 'Configuration ajoutée au clipboard.',
        });
    }
}
