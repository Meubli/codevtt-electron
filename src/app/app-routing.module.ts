import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { HomeRoutingModule } from './home/home-routing.module';
import { HomeGuard } from './core/guards/home.guard';
import { AuthGuard } from './core/guards/auth.guard';
import { RedirectGuard } from './core/guards/redirect.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
    },
    {
        path: 'home',
        loadChildren: () =>
            import('./home/home.module').then((m) => m.HomeModule),
        canActivate: [AuthGuard, HomeGuard],
    },
    {
        path: 'config',
        loadComponent: () =>
            import('./configuration/configuration.component').then(
                (comp) => comp.ConfigurationComponent
            ),
        canActivate: [AuthGuard],
    },
    {
        path: 'help',
        loadComponent: () =>
            import('./helper/helper.component').then(
                (comp) => comp.HelperComponent
            ),
        canActivate: [AuthGuard],
    },
    {
        path: 'login',
        loadChildren: () =>
            import('./login/login.module').then((m) => m.LoginModule),
    },

    {
        path: '**',
        redirectTo: '/home',
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            preloadingStrategy: PreloadAllModules,
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
