import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {CalendarModule} from 'primeng/calendar';

import {HomeRoutingModule} from './home-routing.module';

import {HomeComponent} from './home.component';
import {ButtonModule} from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import {TabViewModule} from 'primeng/tabview';
import {ListboxModule} from 'primeng/listbox';
import {DividerModule} from 'primeng/divider';
import {PanelModule} from 'primeng/panel';
import {SimpleComponent} from './simple/simple.component';
import {RecurringComponent} from './recurring/recurring.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TracksComponent} from './tracks/tracks.component';
import {TrackComponent} from '../core/components/track/track.component';
import {InputTextModule} from 'primeng/inputtext';
import {SkeletonModule} from 'primeng/skeleton';
import {EditionComponent} from './recurring/edition/edition.component';
import {TrackCreationComponent} from './recurring/track-creation/track-creation.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {TemplateSenderComponent} from './recurring/template-sender/template-sender.component';
import {MatCardModule} from '@angular/material/card';
import {RippleModule} from 'primeng/ripple';
import { DialogModule } from 'primeng/dialog';


@NgModule({
    declarations: [
        HomeComponent,
        SimpleComponent,
        RecurringComponent,
        TracksComponent,
        EditionComponent,
        TrackCreationComponent,
        TemplateSenderComponent,
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        ButtonModule,
        CalendarModule,
        TabViewModule,
        FormsModule,
        ReactiveFormsModule,
        ListboxModule,
        TrackComponent,
        DividerModule,
        InputTextModule,
        SkeletonModule,
        PanelModule,
        DragDropModule,
        InputTextModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCardModule,
        RippleModule,
        DropdownModule,
        DialogModule
    ],
})
export class HomeModule {
}
