import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ConfigService} from '../core/services/config.service';
import {CodevttService} from '../core/services/codevtt.service';
import {TemplateService} from '../core/services/template.service';
import { LogoService } from '../core/services/logo.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
    username = '';
    dateClicked: Date;

    modalPaypalVisible = false;

    logo = '';

    constructor(
        private readonly codevttService: CodevttService,
        private readonly router: Router,
        private readonly configService: ConfigService,
        private readonly templateService: TemplateService,
        private readonly logoService: LogoService
    ) {
        if (!configService.isConnected) {
            this.router.navigate(['/login']);
        } else if (!configService.isConfigValid()) {
            this.router.navigate(['/config']);
        } else {
            this.configService.initConfig();
            this.templateService.initTemplateConfig();
        }

        this.username = this.configService.getUsername();
    }

    get isConnected(): boolean {
        return this.configService.isConnected && this.configService.isConfigValid();
    }

    get imageUrl(): string | null {
        return this.codevttService.imageUrl;
    }

    ngOnInit(): void {
        const currentDate = new Date();
        this.logo = this.logoService.retrieveLogoFromMonthAndDay(currentDate.getMonth()+1, currentDate.getDate());
    }

    logout(): void {
        this.codevttService.logout();
    }

    onTracksDayClick(date: Date) {
        this.dateClicked = date;
    }

    public handleMissingImage(event: Event) {
        (event.target as HTMLImageElement).style.display = 'none';
    }
}
