import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    Bug,
    ConfigService,
    Duration,
    Job,
} from '../../core/services/config.service';
import { CodevttService, SimpleDto } from '../../core/services/codevtt.service';
import { Subject } from 'rxjs';
import { MessageService } from 'primeng/api';
import { TrackService } from '../../core/services/track.service';

@Component({
    selector: 'app-simple',
    templateUrl: './simple.component.html',
    styleUrls: ['./simple.component.scss'],
})
export class SimpleComponent {

    formGroup: FormGroup<{
        bug: FormControl<Bug>;
        job: FormControl<Job>;
        dates: FormControl<Date[]>;
        duration: FormControl<Duration>;
    }>;

    loading = false;

    bugs: Bug[] = [];
    jobs: Job[] = [];
    jobsToDisplay: Job[] = [];
    durations: Duration[] = [];
    disableDays = [0,6];



    postTrack$ = new Subject<SimpleDto>();

    constructor(
        private readonly configService: ConfigService,
        private readonly codevttService: CodevttService,
        private readonly trackService: TrackService,
        private readonly messageService: MessageService
    ) {
        this.formGroup = new FormGroup({
            bug: new FormControl<Bug>(null, Validators.required),
            job: new FormControl<Job>(null, Validators.required),
            dates: new FormControl([], Validators.required),
            duration: new FormControl(null, Validators.required),
        });
        this.bugs = this.configService.config.bugs?.length > 0 ? this.configService.config.bugs : this.configService.getAllUserIssues();
        this.jobs = this.configService.getAllJobs().filter(job => job.id !== 1);
        this.jobsToDisplay = this.jobs;
        this.durations = this.configService.config.durations;

        if (configService.config.displayWeekEnd) {
            this.disableDays = [];
        }

        if (this.jobs?.length === 1) {
            this.formGroup.controls.job.patchValue(this.jobs[0]);
        }

    }

    @Input()
    set date(date: Date) {
        if (date != null) {
            this.formGroup.controls.dates.patchValue([date]);
        }
    }

    onBugChange(e: any) {
        if (this.trackService.isBugNA(e.value)) {
            this.jobsToDisplay = [{id: 1, label: 'N/A'}];
            this.formGroup.controls.job.patchValue(this.jobsToDisplay[0]);
        }
        else {
            this.jobsToDisplay = this.jobs;
        }
    }

    send(): void {
        this.loading = true;
        setTimeout(() => {
            this.loading = false;
        }, 2000);

        const simpleDtos: SimpleDto[] = [];
        const twoRequest = this.formGroup.controls.duration.value.value > 1;
        // si duration > 1, faire 2 requetes
        if (this.formGroup.controls.duration.value.value > 1) {
        }

        for (const date of this.formGroup.controls.dates.value) {
            const duration = {...this.formGroup.controls.duration.value};

            const simpleDto = {
                date,
                bug: this.formGroup.controls.bug.value,
                job: this.formGroup.controls.job.value,
                duration,
            };

            simpleDtos.push(simpleDto);
        }

        for (const simpleDto of simpleDtos) {
            this.codevttService.sendSimple(simpleDto).subscribe({
                next: (data) => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Ajouté',
                        detail: `${
                            simpleDto.bug.label
                        } ${simpleDto.date.toLocaleDateString()} durée: ${
                            simpleDto.duration.label
                        }`,
                        life: 5000
                    });
                },
                error: (err) => {
                    console.error(err);
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Erreur',
                        detail: `Erreur lors de l'envoi pour ${
                            simpleDto.bug.label
                        } ${simpleDto.date.toLocaleDateString()} durée ${
                            simpleDto.duration.label
                        }`,
                    });
                },
            });
        }

        this.formGroup.reset();
    }
}
