import {Component} from '@angular/core';
import {TemplateService} from '../../core/services/template.service';
import {Template} from '../../core/models/template';
import {MessageService} from 'primeng/api';
import {ITrack} from '../../core/models/itrack';

@Component({
    selector: 'app-recurring',
    templateUrl: './recurring.component.html',
    styleUrls: ['./recurring.component.scss']
})
export class RecurringComponent {

    templates: Template[];
    tracks: ITrack[];

    constructor(private readonly templateService: TemplateService, private readonly messageService: MessageService) {
        this.templates = [...this.templateService.templates];
        this.tracks = [...this.templateService.tracks];
    }

    onTrackCreated(track: ITrack): void {
        this.templateService.addTrack(track);
        this.tracks = [...this.templateService.tracks];
    }

    onDeleteTrack(trackIndex: number): void {
        this.templateService.deleteTrackByIndex(trackIndex);
        this.tracks = [...this.templateService.tracks];
    }

    onTemplateDelete(template: Template): void {
        this.templateService.deleteTemplate(template);
        this.templates = [...this.templateService.templates];
    }

    createTemplate(template: Template): void {
        try {
            this.templateService.createTemplate(template);
            this.messageService.add({
                severity: 'success',
                summary: 'Template ' + template.identifiant + ' créé.',
            });
            this.templates = [...this.templateService.templates];
        } catch (e) {
            this.messageService.add({
                severity: 'error',
                summary: e.summary,
            });
        }
    }

    updateTemplate(template: Template): void {
        try {
            this.templateService.updateTemplate(template);
            this.messageService.add({
                severity: 'success',
                summary: 'Template ' + template.identifiant + ' modifié.',
            });
            this.templates = [...this.templateService.templates];
        } catch (e) {
            this.messageService.add({
                severity: 'error',
                summary: e.summary,
            });
        }
    }
}
