import {Component, Input} from '@angular/core';
import {TemplateService} from '../../../core/services/template.service';
import {Template} from '../../../core/models/template';
import {ITrack} from '../../../core/models/itrack';
import {CodevttService} from '../../../core/services/codevtt.service';
import {concat, Observable} from 'rxjs';
import {MessageService} from 'primeng/api';

@Component({
    selector: 'app-template-sender',
    templateUrl: './template-sender.component.html',
    styleUrls: ['./template-sender.component.scss']
})
export class TemplateSenderComponent {

    @Input()
    templates: Template[];

    selectedTemplate: Template;

    range: Date[] = [];

    sending = false;


    constructor(
        private readonly templateService: TemplateService,
        private readonly codevttService: CodevttService,
        private readonly messageService: MessageService) {
    }

    send(): void {
        if (this.range[0] > this.range[1]) {
            const temp = this.range[0];
            this.range[0] = this.range[1];
            this.range[1] = temp;
        }
        const currentDate = this.range[0];
        const observables = [];
        do {
            const dayIndex = this.selectedTemplate.days.findIndex(day => day.id === currentDate.getDay());

            if (dayIndex !== -1 && this.selectedTemplate.days[dayIndex].tracks?.length > 0) {
                observables.push(this.sendTracks(this.selectedTemplate.days[dayIndex].tracks, new Date(currentDate)));
            }

            currentDate.setDate(currentDate.getDate() + 1);
        } while (currentDate <= this.range[1]);

        this.sending = true;
        observables.forEach(o => {
            o.subscribe({
                next: (data) => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Ajouté',
                        life: 5000
                    });
                    this.sending = false;
                    this.range = [];
                },
                error: (err) => {
                    console.error(err);
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Erreur',
                        detail: `Erreur lors de l'envoi`,
                    });
                    this.sending = false;
                },
            });
        });
    }

    sendTracks(tracks: ITrack[], date: Date): Observable<any> {
        if (!tracks || !date || tracks.length < 1) return;

        return concat(
            ...tracks.map(track => this.codevttService.sendSimple({...track, date}))
        );

    }

    formValid(): boolean {
        if (!this.range || this.range.length < 2) {
            return false;
        }
        if (this.range[0] === this.range[1]) return false;
        if (!this.selectedTemplate || this.sending) return false;
        return true;
    }
}
