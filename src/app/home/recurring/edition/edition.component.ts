import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Day, Template} from '../../../core/models/template';
import {ITrack} from '../../../core/models/itrack';
import {TemplateService} from '../../../core/services/template.service';
import {
    CdkDragDrop,
    CdkDragEnter,
    CdkDragExit,
    CdkDragStart,
    copyArrayItem,
    moveItemInArray,
} from '@angular/cdk/drag-drop';
import {remove} from 'lodash-es';


@Component({
    selector: 'app-edition',
    templateUrl: './edition.component.html',
    styleUrls: ['./edition.component.scss'],
})
export class EditionComponent implements OnInit {
    @Input()
    update = false;


    @Input()
    templates: Template[];

    @Input()
    tracks: ITrack[];

    @Output()
    templateCreated: EventEmitter<Template> = new EventEmitter<Template>();
    @Output()
    templateUpdated: EventEmitter<Template> = new EventEmitter<Template>();

    @Output()
    templateDeleted: EventEmitter<Template> = new EventEmitter<Template>();

    @Output()
    trackDeleted: EventEmitter<number> = new EventEmitter<number>();

    selectedTemplate: Template;

    templateId: string;

    uniqueId: string;
    dragging = false;

    days: Day[] = [
        {label: 'Lundi', id: 1, tracks: []},
        {label: 'Mardi', id: 2, tracks: []},
        {label: 'Mercredi', id: 3, tracks: []},
        {label: 'Jeudi', id: 4, tracks: []},
        {label: 'Vendredi', id: 5, tracks: []},
    ];

    constructor(private readonly templateService: TemplateService) {
        this.uniqueId = Date.now().toString(36) + Math.floor(Math.pow(10, 12) + Math.random() * 9 * Math.pow(10, 12)).toString(36);
    }

    dayIds() {
        const ids = [];
        for (let i = 0; i < 5; i++) {
            ids.push(this.uniqueId + '-day-' + i);
        }
        return ids;
    }

    ngOnInit() {
        if (this.update && !this.templates) {
            throw new Error('templates have to be provided when update is true');
        }

    }

    onTemplateChange(e: any) {
        this.days = [...e.value?.days];
    }

    onDeleteGlobalTrack(index: number): void {
        this.trackDeleted.emit(index);
    }

    onDeleteTemplate() {
        if (!this.selectedTemplate) return;
        this.templateDeleted.emit(this.selectedTemplate);
        this.selectedTemplate = null;
        this.days = [];
    }

    deleteTrackFromDay(track: ITrack, day: Day): void {
        const index = day.tracks.indexOf(track);
        if (index !== -1) {
            day.tracks.splice(index, 1);
        }
    }

    dropIntoList(event: any): void {
        this.dragging = false;
        document.body.style.cursor = 'auto';
    }

    dropTrack(event: CdkDragDrop<ITrack[]>, day: Day): void {
        if (event.previousContainer === event.container) {
            moveItemInArray(
                event.container.data,
                event.previousIndex,
                event.currentIndex
            );
        } else {
            copyArrayItem(
                event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex
            );
        }
        this.tracks = [...this.tracks];

        remove(this.tracks, {temp: true});

        this.dragging = false;
        document.body.style.cursor = 'auto';
    }

    noReturnPredicate() {
        return false;
    }

    onSourceListExited(event: CdkDragExit<any>) {
        console.log(event.item.data);
        const itemIndex = this.tracks.findIndex(t =>
            t.bug.id === event.item.data.bug.id &&
            t.job.id === event.item.data.job.id &&
            t.duration.value === event.item.data.duration.value
        );
        this.tracks.splice(itemIndex + 1, 0, {
            ...event.item.data,
            temp: true
        });
    }

    onSourceListEntered(event: CdkDragEnter<any>) {
        remove(this.tracks, {temp: true});
    }

    dragFromListStart(event: CdkDragStart<ITrack[]>, index: number): void {
        this.dragging = true;
        document.body.style.cursor = 'grabbing';
    }

    isTemplateValid(): boolean {
        let nbTracks = 0;
        this.days?.forEach((day) => {
            nbTracks += day.tracks.length;
        });

        return nbTracks > 0 && this.templateId?.length > 0;
    }

    createTemplate(): void {
        this.templateCreated.emit({days: this.days, identifiant: this.templateId});
        this.templateId = '';
        this.days = [
            {label: 'Lundi', id: 1, tracks: []},
            {label: 'Mardi', id: 2, tracks: []},
            {label: 'Mercredi', id: 3, tracks: []},
            {label: 'Jeudi', id: 4, tracks: []},
            {label: 'Vendredi', id: 5, tracks: []},
        ];

    }

    updateTemplate() {
        this.templateUpdated.emit({identifiant: this.selectedTemplate.identifiant, days: this.days});
        this.days = [];
        this.selectedTemplate = null;
    }
}
