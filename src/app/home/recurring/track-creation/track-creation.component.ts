import {Component, EventEmitter, Output} from '@angular/core';
import {TrackService} from '../../../core/services/track.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Bug, ConfigService, Duration, Job} from '../../../core/services/config.service';
import {ITrack} from '../../../core/models/itrack';

@Component({
    selector: 'app-track-creation',
    templateUrl: './track-creation.component.html',
    styleUrls: ['./track-creation.component.scss']
})
export class TrackCreationComponent {

    @Output()
    createTrackEvent: EventEmitter<ITrack> = new EventEmitter();

    formGroup: FormGroup<{
        bug: FormControl<Bug>;
        job: FormControl<Job>;
        duration: FormControl<Duration>;
    }>;

    bugs: Bug[] = [];
    jobs: Job[] = [];
    jobsToDisplay: Job[] = [];
    durations: Duration[] = [];

    constructor(
        private readonly trackService: TrackService,
        private readonly configService: ConfigService
    ) {
        this.formGroup = new FormGroup({
            bug: new FormControl<Bug>(null, Validators.required),
            job: new FormControl<Job>(null, Validators.required),
            duration: new FormControl(null, Validators.required),
        });

        this.bugs = this.configService.config.bugs?.length > 0 ? this.configService.config.bugs : this.configService.getAllUserIssues();
        this.jobs = this.configService.getAllJobs().filter(job => job.id !== 1);
        this.jobsToDisplay = this.jobs;
        this.durations = this.configService.config.durations;

        if (this.jobs?.length === 1) {
            this.formGroup.controls.job.patchValue(this.jobs[0]);
        }
    }

    onBugChange(e: any) {
        if (this.trackService.isBugNA(e.value)) {
            this.jobsToDisplay = [{id: 1, label: 'N/A'}];
            this.formGroup.controls.job.patchValue(this.jobsToDisplay[0]);
        } else {
            this.jobsToDisplay = this.jobs;
        }
    }

    create() {
        this.createTrackEvent.emit(this.formGroup.value as ITrack);
        this.formGroup.reset();
    }
}
