import {ChangeDetectorRef, Component, EventEmitter, OnInit, Output,} from '@angular/core';
import {Error, OptionWeek, Track, TrackService,} from '../../core/services/track.service';
import {MessageService} from 'primeng/api';
import {UtilService} from '../../core/services/util.service';
import {ConfigService} from '../../core/services/config.service';
import {weekIdFromDate} from '../../core/tools/dateutils';

@Component({
    selector: 'app-tracks',
    templateUrl: './tracks.component.html',
    styleUrls: ['./tracks.component.scss'],
})
export class TracksComponent implements OnInit {
    @Output()
    dateClickEvent: EventEmitter<Date> = new EventEmitter();

    selectedOptionWeek: OptionWeek;
    selectedOptionYear: number;

    loadingWeek = false;

    shouldDisplayWeekends = false;
    days = [
        {label: 'Lundi', id: 1},
        {label: 'Mardi', id: 2},
        {label: 'Mercredi', id: 3},
        {label: 'Jeudi', id: 4},
        {label: 'Vendredi', id: 5},
    ];
    loading = false;
    loadingTracks: Track[] = [];

    constructor(
        private readonly trackService: TrackService,
        private readonly utilService: UtilService,
        private readonly messageService: MessageService,
        private readonly configService: ConfigService,
        private changeDetectorRef: ChangeDetectorRef
    ) {
    }

    get tracks(): Track[] {
        return this.trackService.tracks;
    }

    get errors(): Error[] {
        return this.trackService.errors;
    }

    get errorsString(): string {
        return `${this.errors.length} Erreur${
            this.errors.length > 1 ? 's' : ''
        }`;
    }

    get optionsWeek(): OptionWeek[] {
        return this.trackService.optionsWeek;
    }

    get optionsYear(): number[] {
        return this.trackService.optionsYear;
    }

    ngOnInit(): void {
        const date = new Date();
        this.loadingWeek = true;
        this.trackService
            .changeWeek(this.utilService.weekNumber(date), date.getFullYear())
            .subscribe(() => {
                this.selectedOptionWeek = {
                    ...this.trackService.selectedOptionWeek,
                };
                this.selectedOptionYear = this.trackService.selectedOptionYear;
                this.loadingWeek = false;
            });

        this.shouldDisplayWeekends = this.configService.config.displayWeekEnd;

        if (this.shouldDisplayWeekends) {
            this.days.push(
                {label: 'Samedi', id: 6},
                {label: 'Dimanche', id: 0}
            );
        }

        this.trackService.selectedWeekChanged.subscribe(() => {
            console.log('selected week changed in component: ', this.trackService.selectedOptionWeek);
            this.selectedOptionWeek = {
                ...this.trackService.selectedOptionWeek,
            };
            this.selectedOptionYear = this.trackService.selectedOptionYear;
        });
    }

    changeWeek(event: { originalEvent: any; value: OptionWeek }): void {
        this.loadingWeek = true;
        this.trackService
            .changeWeek(
                event.value.id,
                this.trackService.timeTrackingSmartyData.year
            )
            .subscribe(() => {
                this.loadingWeek = false;
            });
    }

    changeYear(event: { originalEvent: any; value: number }): void {
        this.loadingWeek = true;
        this.trackService
            .changeWeek(
                this.trackService.timeTrackingSmartyData.weekid,
                event.value
            )
            .subscribe(() => {
                this.loadingWeek = false;
            });
    }

    delete(track: Track): void {
        this.loadingTracks.push(track);
        this.trackService.deleteTrack(track).subscribe({
            next: (page) => {
                const index = this.loadingTracks.indexOf(track);
                if (index >= 0) {
                    this.loadingTracks.splice(index, 1);
                }
                this.messageService.add({
                    severity: 'success',
                    summary: 'Imputation supprimée',
                });
            },
            error: (err) => {
                console.error(err);

                this.messageService.add({
                    severity: 'error',
                    summary: 'Echec suppression',
                });
            },
        });
    }

    totalDay(day: number): number {
        let total = 0;

        for (const track of this.tracks) {
            if (track.date.getDay() === day) {
                let duration = 0;

                if (track.duration.includes(' h')) {
                    duration = Number(
                        track.duration.replace(' h', '').replace(',', '.')
                    );
                } else {
                    duration = Number(track.duration) * 8;
                }
                total += duration;
            }
        }

        return total;
    }

    date(day: number): Date {
        if (!this.trackService.timeTrackingSmartyData) {
            return new Date();
        }
        // eslint-disable-next-line max-len
        return this.trackService.getDateFromWeekdayAndWeekNumber(
            this.trackService.timeTrackingSmartyData.year,
            this.trackService.timeTrackingSmartyData.weekid,
            day
        );
    }

    nextWeek() {
        this.loading = true;
        this.trackService.nextWeek().subscribe({
            next: () => {
                this.loading = false;
                this.selectedOptionWeek = this.trackService.selectedOptionWeek;
            },
        });
    }

    previousWeek() {
        this.loading = true;
        this.trackService.previousWeek().subscribe({
            next: () => {
                this.loading = false;
                this.selectedOptionWeek = this.trackService.selectedOptionWeek;
            },
        });
    }

    isDayHoliday(day: { id: number }): boolean {
        for (const holiday of this.trackService.holidaysOfWeek) {
            if (holiday.dayOfWeekIndex === day.id) {
                return true;
            }
        }
        return false;
    }

    onErrorsClick() {
        if (this.errors.length > 0) {
            this.loading = true;
            const error = this.errors[0];
            this.trackService
                .changeWeek(
                    weekIdFromDate(error.date),
                    error.date.getFullYear()
                )
                .subscribe({
                    next: () => {
                        this.loading = false;
                    },
                });
        }
    }

    onDayClick(dayId: number) {
        this.dateClickEvent.emit(this.date(dayId));
    }

    shouldMarkDayAsError(day: number) {
        if (day === 0 || day === 6) return false;

        if (this.isDayHoliday({id: day})) return false;

        if (this.totalDay(day) >= 8) return false;

        if (!this.trackService.timeTrackingSmartyData) return false;

        const currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        const date = this.trackService.getDateFromWeekdayAndWeekNumber(
            this.trackService.timeTrackingSmartyData.year,
            this.trackService.timeTrackingSmartyData.weekid,
            day
        );
        date.setHours(0, 0, 0, 0);

        return date < currentDate;
    }
}
