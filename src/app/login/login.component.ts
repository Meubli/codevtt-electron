import {Component} from '@angular/core';
import {MessageService} from 'primeng/api';
import {CodevttService} from '../core/services/codevtt.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ConfigService} from '../core/services/config.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
    formGroup: FormGroup<{
        username: FormControl<string>;
        password: FormControl<string>;
        savePassword: FormControl<boolean>;
    }>;

    loading = false;

    constructor(
        private readonly messageService: MessageService,
        private readonly codevttService: CodevttService,
        private readonly configService: ConfigService,
        private readonly router: Router
    ) {
        this.formGroup = new FormGroup({
            username: new FormControl(
                this.configService.getUsername(),
                Validators.required
            ),
            password: new FormControl('', Validators.required),
            savePassword: new FormControl(false)
        });
        this.formGroup.controls.savePassword.setValue(this.configService.shouldSavePassword());

        if (this.formGroup.value.savePassword && this.configService.getPassword()) {
            this.formGroup.controls.password.setValue(this.configService.getPassword());
        }
    }

    login(): void {
        this.configService.setUsername(this.formGroup.value.username);

        this.configService.setSavePassword(this.formGroup.value.savePassword);

        if (this.formGroup.value.savePassword) {
            this.configService.setPassword(this.formGroup.value.password);
        }

        this.loading = true;

        this.codevttService.login(this.formGroup.value.password).subscribe((result) => {
            if (!result || result !== 1) {
                this.loading = false;
                this.messageService.add({
                    severity: 'error',
                    summary: 'Echec Authentification',
                });
            } else {
                this.codevttService.configureUseridAndTeams().subscribe({
                    next: () => {
                        this.loading = false;

                        this.configService.isConnected = true;
                        this.router.navigate(['/home']);
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Succès de l\'authentification',
                        });

                    },
                    error: () => {
                        this.loading = false;
                        this.configService.isConnected = false;
                        console.error('error when loading useridandteams');
                        this.messageService.add({
                            severity: 'error',
                            summary: 'Echec Authentification',
                        });
                    }
                });
            }
        });
    }
}
