import {Injectable} from '@angular/core';
import {weekIdFromDate} from '../tools/dateutils';

@Injectable({
    providedIn: 'root'
})
export class UtilService {

    constructor() {
    }

    public weekNumber(date: Date): number {
        console.log('asked date for weekId: ', date);
        const weekId = weekIdFromDate(date);
        console.log('result: ', weekId);
        return weekId;
    }
}
