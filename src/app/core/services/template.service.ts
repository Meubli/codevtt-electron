import {Injectable} from '@angular/core';
import {ConfigService} from './config.service';
import {ITrack} from '../models/itrack';
import {Template} from '../models/template';

export type TemplateConfig = {
    tracks: ITrack[];
    templates: Template[];
};

@Injectable({
    providedIn: 'root'
})
export class TemplateService {

    constructor(private readonly configService: ConfigService) {
    }

    private _templates: Template[] = [];

    get templates(): Template[] {
        return this._templates;
    }

    private _tracks: ITrack[] = [];

    get tracks(): ITrack[] {
        return this._tracks;
    }

    get templateConfig(): TemplateConfig {
        return {tracks: this._tracks, templates: this._templates};
    }

    public initTemplateConfig() {
        const userid = this.configService.getUserid();
        if (!userid && !isNaN(userid)) throw new Error('userid non présent');

        let allTemplateConfig: { id: number; templateConfig: TemplateConfig }[] = [];
        if (localStorage.getItem('allTemplateConfig')) {
            allTemplateConfig = JSON.parse(localStorage.getItem('allTemplateConfig'));
            const config = allTemplateConfig.find(
                (item) => item.id === userid
            );

            if (config) {
                this._tracks = config.templateConfig.tracks;
                this._templates = config.templateConfig.templates;
            }
        }
    }

    deleteTrackByIndex(index: number): void {
        this._tracks.splice(index, 1);
        this.saveTemplateConfig();
    }

    deleteTemplate(template: Template): void {
        this._templates = this._templates.filter(t => t.identifiant !== template.identifiant);
        this.saveTemplateConfig();
    }

    addTrack(track: ITrack): void {
        if (!track) return;

        this._tracks.push(track);
        this.saveTemplateConfig();
    }

    /**
     * Renvoie true si succès.
     *
     * @param template
     */
    createTemplate(template: Template): void {
        //check if id already exists
        if (this._templates.findIndex(t => t.identifiant === template.identifiant) !== -1) {
            throw new Error('template déja existant');
        }
        this._templates.push(template);
        this.saveTemplateConfig();
    }

    updateTemplate(template: Template): void {
        const index = this._templates.findIndex(t => t.identifiant === template.identifiant);
        if (index === -1) {
            throw new Error('template inexistant');
        }
        this._templates[index] = {...template, identifiant: template.identifiant};
        this.saveTemplateConfig();
    }

    private saveTemplateConfig() {
        const userid = this.configService.getUserid();
        if (!userid) return;

        let allTemplateConfig: { id: number; templateConfig: TemplateConfig }[] = [];
        if (localStorage.getItem('allTemplateConfig')) {
            allTemplateConfig = JSON.parse(localStorage.getItem('allTemplateConfig'));
        }
        const index = allTemplateConfig.findIndex(
            (item) => item.id === userid
        );

        if (index !== -1) {
            allTemplateConfig.splice(index, 1);
        }
        allTemplateConfig.push({id: userid, templateConfig: {templates: this._templates, tracks: this._tracks}});
        localStorage.setItem('allTemplateConfig', JSON.stringify(allTemplateConfig));
    }
}
