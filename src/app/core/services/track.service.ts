import {EventEmitter, Injectable} from '@angular/core';
import {MessageService} from 'primeng/api';
import {APP_CONFIG} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, tap} from 'rxjs';
import {Bug, ConfigService} from './config.service';
import {UtilService} from './util.service';
import {Router} from '@angular/router';

export type Holiday = {
    dayOfWeekIndex: number;
    title: string;
};
export type Track = {
    duration: string;
    date: Date;
    job: string;
    description: string;
    id: number;
    bugId: number;
};


export type Error = {
    date: Date;
    libelle: string;
};

export type TimeTrackingSmartyData = {
    weekid?: number;
    year?: number;
    nbWeeksThisYear?: number;
    nbWeeksPrevYear?: number;
};

export type OptionWeek = {
    id: number;
    label: string;
};

@Injectable({
    providedIn: 'root',
})
export class TrackService {
    tracks: Track[] = [];
    holidaysOfWeek: Holiday[] = [];
    optionsWeek: OptionWeek[] = [];
    optionsYear: number[] = [];
    selectedOptionWeek: OptionWeek;
    selectedOptionYear: number;
    errors: Error[] = [];

    selectedWeekChanged = new EventEmitter<OptionWeek>();
    /**
     * Information sur la semaine séléctionnée, ex: weekid, year etc...
     */
    timeTrackingSmartyData: TimeTrackingSmartyData;
    // Liste des ids pour lequels N/A est le seul job possible.
    private idsNA = [];
    private baseUrl = APP_CONFIG.codevttUrl;

    constructor(
        private readonly messageService: MessageService,
        private readonly http: HttpClient,
        private readonly utilService: UtilService,
        private readonly configService: ConfigService,
        private readonly router: Router
    ) {
        this.initIdsNA();
    }

    /**
     * Détermine si un Bug donnée est un Bug dont le Job est forcément N/A.
     */
    isBugNA(bug: Bug): boolean {
        if (!bug?.id) return false;

        return this.idsNA.includes(bug.id);
    }

    /**
     * Permet d'actualiser les valeurs des tracks, timeTrackingSmartyData etc.. à partir de la page html codevtt.
     *
     * @param page la page  html au format string
     * @returns
     */
    readTracksAndWeekDetails(page: string): void {
        const newDoc = document.implementation.createHTMLDocument('new doc');
        newDoc.getElementsByTagName('html').item(0).innerHTML = page;

        if (!newDoc
            .getElementById('weekTimetrackingTuples')) {
            this.configService.isConnected = false;
            this.router.navigate(['/login']);
            return;
        }

        const tuples = newDoc
            .getElementById('weekTimetrackingTuples')
            .getElementsByClassName('weekTimetrack');
        this.tracks = [];
        for (const tuple of Array.from(tuples)) {
            const id = Number(tuple.getAttribute('data-weektimetrackid'));
            const tds = tuple.getElementsByTagName('td');
            if (tds?.length < 9) {
                console.error('case track bad length');
                return;
            }

            const track: Track = {
                date: new Date(tds.item(2).innerText.substring(0, 10)),
                bugId: Number(tds.item(3).querySelector('a').innerText),
                description: tds.item(7).innerText,
                duration: tds.item(5).innerText,
                job: tds.item(6).innerText,
                id,
            };

            this.tracks.push(track);
        }

        this.tracks.sort((a, b) => a.id - b.id);


        this.optionsWeek = [];
        const optionsWeekHtml = newDoc.querySelectorAll('#weekid option');

        for (const optionWeek of Array.from(optionsWeekHtml)) {
            const weekId = Number(optionWeek.getAttribute('value'));
            const label = optionWeek.textContent;

            this.optionsWeek.push({
                id: weekId,
                label
            });
        }
        const selectedWeek = newDoc.querySelector('#weekid option[selected]');
        if (selectedWeek) {
            this.selectedOptionWeek = this.optionsWeek.find(opt => opt.id === Number(selectedWeek.getAttribute('value')));
        } else {
            this.selectedOptionWeek = this.optionsWeek[0];
        }
        this.optionsYear = [];
        const optionsYearHtml = newDoc.querySelectorAll('#year option');

        for (const optionYear of Array.from(optionsYearHtml)) {
            const year = Number(optionYear.getAttribute('value'));
            this.optionsYear.push(year);
        }
        this.selectedOptionYear = Number(newDoc.querySelector('#year option[selected]').getAttribute('value'));
        this.selectedWeekChanged.emit(this.selectedOptionWeek);

        this.timeTrackingSmartyData = {};

        for (const attribute of [
            'weekid',
            'nbWeeksThisYear',
            'nbWeeksPrevYear',
        ]) {
            let test = '';
            let index = page.indexOf(attribute + ':"') + attribute.length + 2;
            do {
                test = test + page.at(index);
                index++;
            } while (page.at(index) !== '"');
            this.timeTrackingSmartyData[attribute] = Number(test);
        }

        this.timeTrackingSmartyData.year = Number(newDoc.querySelector('#year option[selected]').textContent);

        console.log(this.timeTrackingSmartyData);

        this.readHolidaysOfWeek(newDoc);

        // Récupération des erreurs
        this.errors = [];
        const errorsStringNode = newDoc.querySelector('a#dialog_ConsistencyCheck_link');
        if (errorsStringNode) {
            const errorNodes = newDoc.querySelectorAll('#dialog_ConsistencyCheck table tbody tr');
            for (const errorNode of Array.from(errorNodes)) {
                const dateString = (errorNode.querySelector('td:nth-of-type(3)') as HTMLElement).innerText;
                const libelle = (errorNode.querySelector('td:nth-of-type(6)') as HTMLElement).innerText;

                this.errors.push({
                    date: new Date(dateString),
                    libelle
                });
            }
            console.log(this.errors);

        }
    }

    readHolidaysOfWeek(documentPage: Document) {
        // on recupere les case indicants les jours de la semaine dans le header du tableau
        const weekDaysHeader = documentPage.querySelectorAll('#weekTaskDetails th.dayHeader');

        this.holidaysOfWeek = [];
        weekDaysHeader.forEach((node, index) => {
            if (node.classList.contains('holyDay') && index < 5) {
                this.holidaysOfWeek.push(
                    {
                        dayOfWeekIndex: index + 1,
                        title: ''
                    }
                );
            }
        });
    }

    getBug(bugId: number): Bug {
        const indexConfig = this.configService.config.bugs ? this.configService.config.bugs.findIndex(bug => bug.id === bugId) : -1;

        if (indexConfig !== -1) {
            return this.configService.config.bugs[indexConfig];
        }

        const index = this.configService.getAllUserIssues().findIndex(bug => bug.id === bugId);

        if (indexConfig !== -1) {
            return this.configService.getAllUserIssues()[indexConfig];
        }

        return null;
    }

    deleteTrack(track: Track): Observable<string> {
        const body = new URLSearchParams();
        body.set('action', 'deleteTrack');
        body.set('trackid', String(track.id));
        body.set('weekid', String(this.utilService.weekNumber(track.date)));

        const headers = new HttpHeaders().set(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );

        return this.http
            .post(
                `${this.baseUrl}/codevtt/timetracking/time_tracking.php`,
                body.toString(),
                {headers, withCredentials: true, responseType: 'text'}
            )
            .pipe(
                tap((page) => {
                    this.readTracksAndWeekDetails(page);
                })
            );
    }

    changeWeek(weekId: number, year: number): Observable<string> {
        const body = new URLSearchParams();
        body.set('weekid', String(weekId));
        body.set('year', String(year));
        body.set('userid', String(this.configService.getUserid()));

        const headers = new HttpHeaders().set(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );

        return this.http
            .post(
                `${this.baseUrl}/codevtt/timetracking/time_tracking.php`,
                body.toString(),
                {headers, withCredentials: true, responseType: 'text'}
            )
            .pipe(
                tap((page) => {
                    this.readTracksAndWeekDetails(page);
                })
            );
    }

    nextWeek(): Observable<string> {
        const nWY = this.nextWeekIdYear(this.timeTrackingSmartyData.weekid, this.timeTrackingSmartyData.year);
        return this.changeWeek(nWY.weekid, nWY.year);
    }

    previousWeek(): Observable<string> {
        const pWY = this.previousWeekIdYear(this.timeTrackingSmartyData.weekid, this.timeTrackingSmartyData.year);
        return this.changeWeek(pWY.weekid, pWY.year);
    }

    nextWeekIdYear(weekid: number, year: number): { weekid: number; year: number } {
        if (weekid + 1 <= this.timeTrackingSmartyData.nbWeeksThisYear) {
            return {weekid: weekid + 1, year};
        }
        return {weekid: 1, year: year + 1};
    }

    previousWeekIdYear(weekid: number, year: number): { weekid: number; year: number } {
        if (1 === weekid) {
            return {weekid: this.timeTrackingSmartyData.nbWeeksPrevYear, year: year - 1};
        } else {
            return {weekid: weekid - 1, year};
        }
    }

    getDateFromWeekdayAndWeekNumber(year: number, weekNumber: number, dayOfWeek: number): Date {
        if (dayOfWeek === 0) {
            weekNumber = (weekNumber % 53) + 1;
        }
        // Vérifier si le numéro de semaine est valide (entre 1 et 52/53)
        if (weekNumber < 1 || weekNumber > 53) {
            throw new Error('Numéro de semaine invalide');
        }

        // Vérifier si le jour de la semaine est valide (entre 0 pour dimanche et 6 pour samedi)
        if (dayOfWeek < 0 || dayOfWeek > 6) {
            throw new Error('Jour de la semaine invalide');
        }

        // Créer une date pour le premier jour de l'année
        const firstDayOfYear = new Date(year, 0, 1);

        // Déterminer le jour de la semaine du premier jour de l'année (0 pour dimanche, 1 pour lundi, etc.)
        const firstDayOfWeek = firstDayOfYear.getDay();

        // Calculer le nombre de jours à ajouter pour atteindre le jour de la semaine spécifié
        const daysToAdd = dayOfWeek - firstDayOfWeek;

        // Ajouter le nombre de jours pour obtenir le jour de la semaine spécifié
        firstDayOfYear.setDate(firstDayOfYear.getDate() + daysToAdd);

        // Ajouter le nombre de semaines pour atteindre la semaine spécifiée
        firstDayOfYear.setDate(firstDayOfYear.getDate() + (weekNumber - 1) * 7);

        return firstDayOfYear;
    }

    private initIdsNA() {
        for (let i = 1; i <= 20; i++) {
            this.idsNA.push(i);
        }
        this.idsNA.push(8863, 22, 23, 24, 25, 26, 30, 31, 32, 33, 1336, 1337, 7635, 8228, 32030);
    }
}
