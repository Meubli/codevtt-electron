import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, switchMap, tap} from 'rxjs';
import {APP_CONFIG} from '../../../environments/environment';

export type Bug = {
    id?: number;
    label?: string;
    index?: number;
    color?: string;
};

export type Job = {
    id?: number;
    label?: string;
    index?: number;
};

export type Duration = {
    value?: number;
    label?: string;
    index?: number;
};

export type Team = {
    id?: number;
    label?: string;
    issues?: number[];
};

export type Config = {
    durations?: Duration[];
    bugs?: Bug[];
    displayWeekEnd?: boolean;
};

@Injectable({
    providedIn: 'root',
})
export class ConfigService {
    config: Config = {};
    private baseUrl = APP_CONFIG.codevttUrl;
    private allIssues: Bug[] = [];
    private allJobs: Job[] = [];
    private allDurations: Duration[] = [];
    private teams: Team[] = [];

    constructor(private readonly http: HttpClient) {
        this.initConfig();
    }

    get isConnected(): boolean {
        return JSON.parse(localStorage.getItem('isConnected'));
    }

    set isConnected(c: boolean) {
        localStorage.setItem('isConnected', JSON.stringify(c));
    }

    public initConfig(): void {
        const durations = [];
        for (let i = 1; i <= 48; i++) {
            const mins = i * 15;
            const hours = Math.floor(mins / 60);
            durations.push({
                value: i * 0.03125,
                label: `${hours}h${mins % 60}`,
            });
        }

        this.allDurations = [];
        for (let i = durations.length - 1; i > 0; i--) {
            this.allDurations.push(durations.at(i));
        }

        const canLoadConf = this.getUserid() && !isNaN(this.getUserid());
        if (!canLoadConf) {
            return;
        }

        let allconfig: { id: number; config: Config }[] = [];
        if (localStorage.getItem('allConfig')) {
            allconfig = JSON.parse(localStorage.getItem('allConfig'));

            const config = allconfig.find(
                (item) => item.id === this.getUserid()
            );

            if (config) {
                this.config = config.config;
            }
        }

        if (!this.config.durations || this.config.durations.length === 0) {
            this.config.durations = this.allDurations;
            this.saveConfig();
        }

        // on charge les issues liées à l'utilisateur actuel
        if (localStorage.getItem('allBugs')) {
            const issues: { id: number; bugs: Bug[] }[] = JSON.parse(
                localStorage.getItem('allBugs')
            );
            const userIssues = issues.filter(
                (item) => item.id === this.getUserid()
            );

            if (userIssues && userIssues.length > 0) {
                this.allIssues = userIssues[0].bugs;
            }
        }

        if (localStorage.getItem('teams')) {
            const teams: { id: number; teams: Team[] }[] = JSON.parse(
                localStorage.getItem('teams')
            );
            const userTeams = teams.filter(
                (item) => item.id === this.getUserid()
            );

            if (userTeams && userTeams.length > 0) {
                this.teams = userTeams[0].teams;
            }
        }

        if (localStorage.getItem('allJobs')) {
            this.allJobs = JSON.parse(localStorage.getItem('allJobs'));
        } else {
            this.setAllJobs([
                {
                    id: 1,
                    label: 'N/A',
                },
                {
                    id: 28,
                    label: 'Architecte',
                },
                {
                    id: 33,
                    label: 'RRP',
                },
                {
                    id: 32,
                    label: 'CDS',
                },
                {
                    id: 24,
                    label: 'Developpeur',
                },
                {
                    id: 23,
                    label: 'Chef de Projet (RCP/RTS)',
                },
                {
                    id: 27,
                    label: 'Administrateur Exploitant'
                },
                {
                    id: 34,
                    label: 'Personnel Standardisation'
                },
                {
                    id: 32,
                    label: 'Chef de section - pôle'
                },
                {
                    id: 29,
                    label: 'Analyste technico-fonctionnel'
                },
                {
                    id: 31,
                    label: 'Administrateur de données'
                },
                {
                    id: 25,
                    label: 'Traitant SSI'
                },
                {
                    id: 26,
                    label: 'Traitant marchés - budgets'
                },
                {
                    id: 22,
                    label: 'Chef de centre-CIV-bureau'
                }
            ]);
        }
    }

    getAllDurations(): Duration[] {
        return this.allDurations;
    }

    getAllUserIssues(): Bug[] {
        return this.allIssues;
    }

    addToAllUserIssues(bug: Bug): void {
        if (!this.getUserid() && isNaN(this.getUserid())) {
            throw new Error('userid erroné: ' + this.getUserid());
        }
        if (this.allIssues.find((b) => b.id === bug.id)) {
            return;
        }
        this.allIssues.push(bug);

        let allBugs: { id: number; bugs: Bug[] }[] = [];
        if (localStorage.getItem('allBugs')) {
            allBugs = JSON.parse(localStorage.getItem('allBugs'));
        }

        const index = allBugs.findIndex((item) => item.id === this.getUserid());

        if (index !== -1) {
            allBugs.splice(index, 1);
        }

        allBugs.push({
            id: this.getUserid(),
            bugs: this.allIssues,
        });

        localStorage.setItem('allBugs', JSON.stringify(allBugs));
    }

    getAllJobs(): Bug[] {
        return this.allJobs;
    }

    addToAllJobs(job: Job): void {
        if (this.allJobs.findIndex((b) => b.id === job.id) !== -1) {
            return;
        }
        this.allJobs.push(job);
        localStorage.setItem('allJobs', JSON.stringify(this.allJobs));
    }

    setAllJobs(jobs: Job[]): void {
        this.allJobs = jobs;
        localStorage.setItem('allJobs', JSON.stringify(this.allJobs));
    }

    setUserTeams(teams: Team[]) {
        if (!this.getUserid() && isNaN(this.getUserid())) {
            throw new Error('userid erroné: ' + this.getUserid());
        }
        this.teams = teams;

        let allTeams: { id: number; teams: Team[] }[] = [];
        if (localStorage.getItem('teams')) {
            allTeams = JSON.parse(localStorage.getItem('teams'));
        }

        const index = allTeams.findIndex(
            (item) => item.id === this.getUserid()
        );

        if (index !== -1) {
            allTeams.splice(index, 1);
        }

        allTeams.push({
            id: this.getUserid(),
            teams: this.teams,
        });
        localStorage.setItem('teams', JSON.stringify(allTeams));
    }

    /**
     * supprime l'ancienne si id présent
     *
     * @param team
     */
    addTeam(team: Team): void {
        const index = this.teams.findIndex((t) => t.id === team.id);
        if (index >= 0) {
            this.teams.splice(index, 1);
        }
        this.teams.push(team);
        this.setUserTeams(this.teams);
    }

    getTeams(): Team[] {
        return this.teams;
    }

    public isConfigValid(): boolean {
        if (!this.getTeams() || !this.getAllUserIssues()) {
            return false;
        }
        if (
            this.getAllJobs()?.length === 0 ||
            this.config?.durations?.length === 0
        ) {
            return false;
        }
        return true;
    }

    public areTeamIssuesFetched(): boolean {
        return (
            this.getTeams() &&
            this.getTeams().findIndex((t) => !t.issues) === -1
        );
    }

    public saveConfig(): void {
        let allconfig: { id: number; config: Config }[] = [];
        if (localStorage.getItem('allConfig')) {
            allconfig = JSON.parse(localStorage.getItem('allConfig'));
        }

        const index = allconfig.findIndex(
            (item) => item.id === this.getUserid()
        );

        if (index !== -1) {
            allconfig.splice(index, 1);
        }
        allconfig.push({id: this.getUserid(), config: this.config});
        localStorage.setItem('allConfig', JSON.stringify(allconfig));
    }

    public setConfigItem(key: string, value: any): void {
        this.config[key] = value;

        this.saveConfig();
    }

    public setConfig(config: Config): void {
        this.config = config;
        this.config.durations.sort((a, b) => a.index - b.index);
        this.saveConfig();
    }

    public setUserid(userid: number): void {
        localStorage.setItem('userid', String(userid));
    }

    public getUserid(): number {
        return Number(localStorage.getItem('userid'));
    }

    public setUsername(username: string): void {
        localStorage.setItem('usx', btoa(username));
    }

    public getUsername(): string {
        return localStorage.getItem('usx')
            ? atob(localStorage.getItem('usx'))
            : null;
    }

    public setPassword(password: string): void {
        localStorage.setItem('panda', btoa(password));
    }

    public getPassword(): string {
        return localStorage.getItem('panda')
            ? atob(localStorage.getItem('panda'))
            : null;
    }

    public setSavePassword(savePassword: boolean): void {
        localStorage.setItem('savePwd', String(savePassword));
    }

    public shouldSavePassword(): boolean {
        return localStorage.getItem('savePwd')
            ? /^true$/i.test(localStorage.getItem('savePwd'))
            : false;
    }

    public fetchTeamsBugsAndJobs(): Observable<void> {
        return null;
    }

    /**
     * recupere les bugs et jobs pour la team en session acutelle
     */
    public fetchBugsAndJobsForTeam(
        team: Team
    ): Observable<{ availableIssues: any; availableJobs: any }> {
        const body = new URLSearchParams();
        body.set('action', 'getIssuesAndDurations');
        body.set('projectid', '0');
        body.set('managedUserid', String(this.getUserid()));

        const headers = new HttpHeaders().set(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );
        return this.http
            .get(`${this.baseUrl}/codevtt/timetracking/time_tracking.php`, {
                params: new HttpParams().set('teamid', team.id),
                withCredentials: true, responseType: 'text'
            })
            .pipe(
                tap(() => {
                    console.log('reception team:' + team.id);
                }),
                switchMap(() =>
                    this.http
                        .post<{ availableIssues: any; availableJobs: any }>(
                            `${this.baseUrl}/codevtt/timetracking/time_tracking_ajax.php`,
                            body,
                            {
                                headers,
                                withCredentials: true,
                            }
                        )
                        .pipe(
                            tap((data) => {
                                console.log('reception issues team:' + team.id);
                                team.issues = [];

                                for (const issue of Object.keys(
                                    data.availableIssues
                                ).map((key) => data.availableIssues[key])) {
                                    this.addToAllUserIssues({
                                        id: Number(issue.id),
                                        label: issue.summary,
                                    });
                                    team.issues.push(Number(issue.id));
                                }

                                this.addTeam(team);
                            })
                        )
                )
            );
        return;
    }
}
