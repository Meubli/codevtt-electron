import { Injectable } from '@angular/core';

type IntervalLogo = {
    intervals: { month: number; day: number }[][];
    logos: string[];
};

const MAP_INTERVALS_LOGOS: IntervalLogo[] = [
    {
        intervals: [
            [
                { month: 3, day: 10 },
                { month: 3, day: 10 },
            ],
        ],
        logos: ['assets/icons/vello.jpg'],
    },
    {
        intervals: [
            [
                { month: 2, day: 13 },
                { month: 2, day: 15 },
            ],
        ],
        logos: ['assets/icons/lazyvttValentine.png'],
    },
    {
        intervals: [
            [
                { month: 10, day: 25 },
                { month: 11, day: 5 },
            ],
        ],
        logos: ['assets/icons/lazyvttHaloween.png'],
    },
    {
        intervals: [
            [
                { month: 12, day: 10 },
                { month: 1, day: 10 },
            ],
        ],
        logos: ['assets/icons/hiver.png', 'assets/icons/lazyvttxmas.png'],
    },
    {
        intervals: [
            [
                { month: 5, day: 20 },
                { month: 9, day: 15 },
            ],
        ],
        logos: ['assets/icons/lazyvttSummer.png'],
    },
    {
        intervals: [
            [
                { month: 3, day: 20 },
                { month: 5, day: 19 },
            ],
        ],
        logos: ['assets/icons/lazyVttEaster.png'],
    },
    {
        intervals: [
            [
                { month: 9, day: 16 },
                { month: 12, day: 9 },
            ],
        ],
        logos: ['assets/icons/autumn.png'],
    },
];

@Injectable({
    providedIn: 'root',
})
export class LogoService {
    constructor() {}

    retrieveLogoFromMonthAndDay(month: number, day: number): string {
        for (const intervalLogo of MAP_INTERVALS_LOGOS) {
            if (this.isInInterval({ month, day }, intervalLogo.intervals[0])) {
                return intervalLogo.logos[
                    Math.floor(Math.random() * intervalLogo.logos.length)
                ];
            }
        }

        return 'assets/icons/favicon.512x512.png';
    }

    isInInterval(
        dateToCheck: { month: number; day: number },
        interval: { month: number; day: number }[]
    ): boolean {
        if (
            interval[0].month > interval[1].month ||
            (interval[0].month === interval[1].month &&
                interval[0].day > interval[1].day)
        ) {
            interval[1].month += 12;
        }

        // Vérifiez si la dateToCheck est après ou égale à la date de début de l'intervalle
        const isAfterStart =
            dateToCheck.month > interval[0].month ||
            (dateToCheck.month === interval[0].month &&
                dateToCheck.day >= interval[0].day);

        // Vérifiez si la dateToCheck est avant ou égale à la date de fin de l'intervalle
        const isBeforeEnd =
            dateToCheck.month < interval[1].month ||
            (dateToCheck.month === interval[1].month &&
                dateToCheck.day <= interval[1].day);

        // La date est dans l'intervalle si elle est après le début et avant la fin
        return isAfterStart && isBeforeEnd;
    }
}
