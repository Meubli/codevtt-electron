import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Bug, ConfigService, Duration, Job, Team} from './config.service';
import {concatMap, map, Observable, switchMap, tap} from 'rxjs';
import {APP_CONFIG} from '../../../environments/environment';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';
import {TrackService} from './track.service';
import {UtilService} from './util.service';

export type SimpleDto = {
    bug: Bug;
    job: Job;
    date: Date;
    duration: Duration;
};

@Injectable({
    providedIn: 'root',
})
export class CodevttService {
    private baseUrl = APP_CONFIG.codevttUrl;

    imageUrl: string | null = null;

    constructor(
        private readonly http: HttpClient,
        private readonly configService: ConfigService,
        private readonly datePipe: DatePipe,
        private readonly router: Router,
        private readonly messageService: MessageService,
        private readonly trackService: TrackService,
        private readonly utilService: UtilService
    ) {
    }


    public login(password: string): Observable<number> {
        const body = new URLSearchParams();
        body.set('codev_login', this.configService.getUsername());
        body.set('codev_passwd', password);
        body.set('action', 'login');

        const headers = new HttpHeaders().set(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );

        return this.http.post<number>(
            `${this.baseUrl}/codevtt/login.php`,
            body.toString(),
            {headers, withCredentials: true}
        );
    }

    public sendSimple(simpleDto: SimpleDto): Observable<any> {
        const body = new URLSearchParams();
        body.set('bugid', String(simpleDto.bug.id));
        body.set('userid', String(this.configService.getUserid()));
        body.set('duree', String(simpleDto.duration.value <= 1 ? simpleDto.duration.value : 1));
        body.set('date', this.datePipe.transform(simpleDto.date, 'yyyy-MM-dd'));
        body.set('job', String(simpleDto.job.id));
        body.set('action', 'addTrack');
        body.set('projectid', '1');
        body.set('year', String(simpleDto.date.getFullYear()));
        body.set('weekid', String(this.utilService.weekNumber(simpleDto.date)));

        const headers = new HttpHeaders().set(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );

        if (simpleDto.duration.value <= 1) {
            return this.http
                .post(
                    `${this.baseUrl}/codevtt/timetracking/time_tracking.php`,
                    body.toString(),
                    {headers, withCredentials: true, responseType: 'text'}
                )
                .pipe(
                    tap((page) => {
                        this.trackService.readTracksAndWeekDetails(page);
                    })
                );
        } else {
            return this.http
                .post(
                    `${this.baseUrl}/codevtt/timetracking/time_tracking.php`,
                    body.toString(),
                    {headers, withCredentials: true, responseType: 'text'}
                )
                .pipe(
                    concatMap(() => this.sendSimple({
                        ...simpleDto,
                        duration: {...simpleDto.duration, value: simpleDto.duration.value - 1}
                    }))
                );
        }

    }

    public configureUseridAndTeams(): Observable<any> {
        return this.http
            .get(`${this.baseUrl}/codevtt/timetracking/time_tracking.php`, {
                withCredentials: true,
                responseType: 'text',
            })
            .pipe(
                map((page) => {
                    this.trackService.readTracksAndWeekDetails(page);
                    let userid = '';
                    let index = page.indexOf('userid:"') + 8;
                    do {
                        userid = userid + page.at(index);
                        index++;
                    } while (page.at(index) !== '"');

                    if (!/^-?\d+$/.test(userid)) {
                        throw new Error('impossible de lire le userid');
                    }

                    this.configService.setUserid(Number(userid));

                    const newDoc =
                        document.implementation.createHTMLDocument('new doc');
                    newDoc.getElementsByTagName('html').item(0).innerHTML =
                        page;

                    const identity = newDoc.querySelector('#content h2').innerHTML.split(' [')[0].replace(' ', '_');

                    this.imageUrl = `https://portail-smsif.intradef.gouv.fr/SMSIFRH_Personnel/${identity}.jpg`;

                    const options = newDoc
                        .getElementById('teamid')
                        .getElementsByTagName('option');

                    const teams: Team[] = [];
                    let selectedTeam: Team = null;
                    for (const option of Array.from(options)) {
                        if (option.getAttribute('value') !== '0') {
                            const team = {
                                id: Number(option.getAttribute('value')),
                                label: option.innerText,
                            };
                            teams.push(team);
                            if (option.hasAttribute('selected')) {
                                selectedTeam = team;
                            }
                        }
                    }

                    this.configService.setUserTeams(teams);

                    let changement = false;
                    for (const team of teams) {
                        let idPresent = false;
                        for (const configTeam of this.configService.getTeams()) {
                            if (configTeam.id === team.id) {
                                idPresent = true;
                            }
                        }

                        if (!idPresent) {
                            changement = true;
                        }
                    }

                    // si il y a eu un changement dans les teams, on réaffecte
                    if (changement) {
                        this.configService.setUserTeams(teams);
                    }

                    // si les issues des equipes n'ont pas encore étaient fetch, on les fetch
                    let requestTeam = null;
                    for (const team of this.configService.getTeams()) {
                        if (
                            !this.configService
                                .getTeams()
                                .find((t) => t.id === team.id)
                                .issues
                        ) {
                            if (!requestTeam) {
                                requestTeam = this.configService
                                    .fetchBugsAndJobsForTeam(team);
                            } else {
                                requestTeam = requestTeam.pipe(switchMap(() => this.configService
                                    .fetchBugsAndJobsForTeam(team)));
                            }


                        }
                    }

                    requestTeam.subscribe(() => {
                    });


                    this.configService.initConfig();

                    return Number(userid);
                })
            );
    }

    public logout(): void {
        this.configService.isConnected = false;
        this.router.navigate(['/login']);
    }
}
