import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { Track, TrackService } from '../../services/track.service';
import { SkeletonModule } from 'primeng/skeleton';
import { ITrack } from '../../models/itrack';

export interface ITrackDisplay {
    job: string;
    duration: string;
    bug: string;
    bugId: number;
}
@Component({
  selector: 'app-track',
  standalone: true,
  imports: [CommonModule, ButtonModule, SkeletonModule],
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent {

    @Input() track: ITrackDisplay;

    @Input() draggable = false;
    @Input() dragging = true;

    @Input() loading = false;

    @Output() deleteEvent = new EventEmitter();

    constructor(private readonly trackService: TrackService) {}

    get color(): string {
        if (!this.trackService.getBug(this.track.bugId) || this.trackService.getBug(this.track.bugId).color?.length === 0) {
            return 'var(--surface-b)';
        }
        return this.trackService.getBug(this.track.bugId).color;
    }

    delete(): void {
        this.deleteEvent.emit();
    }

}
