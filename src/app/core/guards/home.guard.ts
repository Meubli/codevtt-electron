import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanActivateChild,
    Router,
    RouterStateSnapshot,
    UrlTree,
} from '@angular/router';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { ConfigService } from '../services/config.service';
import { CodevttService } from '../services/codevtt.service';

@Injectable({
    providedIn: 'root',
})
export class HomeGuard implements CanActivate, CanActivateChild {
    constructor(
        private router: Router,
        private readonly configService: ConfigService,
        private readonly messageService: MessageService
    ) {}

    canActivateChild(
        childRoute: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | boolean
        | UrlTree
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree> {
        return this.canActivate(childRoute, state);
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        if (!this.configService.isConfigValid()) {
            this.messageService.add({
                severity: 'warn',
                summary: 'Configuration Conseillée',
            });
            this.router.navigate(['/config']);

            return false;
        }
        return true;
    }
}
