import {getWeeksInYear, nextWeek, weekIdFromDate} from './dateutils';
//import 'jasmine';

describe('weekIdFromDate', () => {
    it('should return the correct week number for a date in January', () => {
        const date = new Date(2024, 0, 1); // January 1, 2022
        expect(weekIdFromDate(date)).toBe(1);
    });

    it('should return the correct week number for a date in December 2026', () => {
        const date = new Date(2026, 11, 30); // January 1, 2022
        expect(weekIdFromDate(date)).toBe(53);
    });

    it('should return the correct week number for a date in December', () => {
        const date = new Date(2024, 11, 31); // December 31, 2024
        expect(weekIdFromDate(date)).toBe(1);
    });

    it('should return the correct week number for a date in the middle of the year', () => {
        const date = new Date(2022, 5, 15); // June 15, 2022
        expect(weekIdFromDate(date)).toBe(24);
    });

    it('should return the correct week number for a date on a Sunday', () => {
        const date = new Date(2022, 0, 2); // January 2, 2022 (Sunday)
        expect(weekIdFromDate(date)).toBe(52);
    });

    it('should return the correct week number for a date on a Monday', () => {
        const date = new Date(2022, 0, 3); // January 3, 2022 (Monday)
        expect(weekIdFromDate(date)).toBe(1);
    });

    it('should return the correct week number for a date in a leap year', () => {
        const date = new Date(2020, 1, 29); // February 29, 2024 (leap year)
        expect(weekIdFromDate(date)).toBe(9);
    });

    it('should return the correct week number for a date in a non-leap year', () => {
        const date = new Date(2024, 1, 28); // February 28, 2024 (non-leap year)
        expect(weekIdFromDate(date)).toBe(9);
    });

});

describe(('getWeeksInYear'), () => {
    it('should return the correct number of weeks in a year', () => {
        const year = 2022;
        expect(getWeeksInYear(year)).toBe(52);
    });
    it('should return the correct number of weeks in a year 53 weaks 2020', () => {
        const year = 2020;
        expect(getWeeksInYear(year)).toBe(53);
    });

    it('should return the correct number of weeks in a year 53 weaks 2026', () => {
        const year = 2026;
        expect(getWeeksInYear(year)).toBe(53);
    });

    it('should return the correct number of weeks in a year 52 weaks 2024', () => {
        const year = 2024;
        expect(getWeeksInYear(year)).toBe(52);
    });
});

describe('nextWeek', () => {
    it('should return the next week in the same year', () => {
        const weekInfo = {week: 1, year: 2022};
        const result = nextWeek(weekInfo);
        expect(result).toEqual({week: 2, year: 2022});
    });

    it('should return the next week in the same year dec', () => {
        const weekInfo = {week: 49, year: 2024};
        const result = nextWeek(weekInfo);
        expect(result).toEqual({week: 50, year: 2024});
    });

    it('should return the next week in the next year', () => {
        const weekInfo = {week: 52, year: 2024};
        const result = nextWeek(weekInfo);
        expect(result).toEqual({week: 1, year: 2025});
    });

    it('should return the next week 53', () => {
        const weekInfo = {week: 52, year: 2020};
        const result = nextWeek(weekInfo);
        expect(result).toEqual({week: 53, year: 2020});
    });

    it('should return the first week of the next year when given the last week of the current year', () => {
        const weekInfo = {week: 52, year: 2022};
        const result = nextWeek(weekInfo);
        expect(result).toEqual({week: 1, year: 2023});
    });

    it('should return the correct week number for a date in a leap year', () => {
        const weekInfo = {week: 9, year: 2020};
        const result = nextWeek(weekInfo);
        expect(result).toEqual({week: 10, year: 2020});
    });

    it('should return the correct week number for a date in a non-leap year', () => {
        const weekInfo = {week: 8, year: 2021};
        const result = nextWeek(weekInfo);
        expect(result).toEqual({week: 9, year: 2021});
    });


});
