export function weekIdFromDate(date: Date): number {
    let d = date;
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    // Get first day of year
    const yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    // Calculate full weeks to nearest Thursday
    const weekNo = Math.ceil((((d.getTime() - yearStart.getTime()) / 86400000) + 1) / 7);
    // Return array of year and week number
    return weekNo;
}

export function nextWeek(weekInfo: { week: number; year: number }): { week: number; year: number } {

    const {week, year} = weekInfo;
    const currentYearWeeks = getWeeksInYear(year);
    console.log('weeks ' + currentYearWeeks + ' in ' + year);
    if (week < currentYearWeeks) {
        return {week: week + 1, year};
    } else {
        return {week: 1, year: year + 1};
    }
}

export function getWeeksInYear(year: number): number {
    return weekIdFromDate(new Date(year, 11, 31));
}

