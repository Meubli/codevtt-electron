import {ITrack} from './itrack';

export type Day = {
    label: string;
    id: number;
    tracks: ITrack[];
};
export type Template = {
    identifiant: string;
    days: Day[];
};
