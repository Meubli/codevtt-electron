import {Bug, Duration, Job} from '../services/config.service';

export interface ITrack {
    bug: Bug;
    job: Job;
    duration: Duration;
    temp?: boolean;
}
