export const APP_CONFIG = {
  production: true,
  environment: 'PROD',
  codevttUrl: 'https://rosie.intradef.gouv.fr'
};
